{-# language GeneralizedNewtypeDeriving #-}
module Bubblewrap (
  -- * Types and Machinery
  BubbleM(..)
  , SeLabel
  , bwrap
  -- * General options
  , parseArgs
  -- * Options related to kernel namespaces
  , shareNet
  , unshareUser
  , unshareUserTry
  , unshareIpc
  , unsharePid
  , unshareNet
  , unshareUts
  , unshareCgroup
  , unshareCgroupTry
  , unshareAll
  , useUID
  , useGID
  , useHostname
  -- * Options about environment setup
  , chdir
  , setEnv
  , unsetEnv
  -- * Options for monitoring the sandbox from the outside
  , lockFile
  , syncFd
  -- * Filesystem related options
  , bind
  , devBind
  , roBind
  , remountRo
  , proc
  , dev
  , tmpfs
  , mqueue
  , dir
  , file
  , bindData
  , roBindData
  , symlink
  -- * Lockdown Options
  , seccomp
  , execLabel
  , fileLabel
  , blockFd
  , infoFd
  , newSession
  , dieWithParent
  -- * Helpers
  , diag
) where

import System.Posix.Process (executeFile)
import System.Posix.Types (Fd,UserID,GroupID)
import Control.Monad.Writer.Class
import Control.Monad.IO.Class
import Control.Monad.Writer

type SeLabel = String

newtype BubbleM a = BubbleM (WriterT [String] IO a) 
  deriving (Functor,Applicative,Monad,MonadWriter [String],MonadIO)

bwrap :: BubbleM a -> FilePath -> [String] -> IO ()
bwrap (BubbleM setup) proc args = do
  (_,bubbleArgs) <- runWriterT setup
  executeFile "/usr/bin/bwrap"
    False (bubbleArgs ++ proc : args) Nothing

diag :: (a -> a -> b) -> a -> b
diag f x = f x x

-- Helpers:

opt0 :: String -> BubbleM ()
opt0 s0 = tell $ pure $ "--" ++ s0

opt1 :: String -> String -> BubbleM ()
opt1 s0 s1 = tell $ [ "--" ++ s0, s1 ]

opt2 :: String -> String -> String -> BubbleM ()
opt2 s0 s1 s2 = tell $ [ "--" ++ s0, s1, s2 ] 

unshareOpt :: String -> BubbleM ()
unshareOpt s = opt0 $ "unshare-" ++ s

shareOpt :: String -> BubbleM ()
shareOpt s = opt0 $ "share-" ++ s


-- General options:

parseArgs :: Fd -> BubbleM ()
parseArgs fd = opt1 "args" (show fd)

-- Options related to kernel namespaces:

shareNet :: BubbleM ()
shareNet = shareOpt "net"

unshareUser ::  BubbleM ()
unshareUser = unshareOpt "user"

unshareUserTry :: BubbleM ()
unshareUserTry = unshareOpt "user-try"

unshareIpc :: BubbleM ()
unshareIpc = unshareOpt "ipc"

unsharePid :: BubbleM ()
unsharePid = unshareOpt "pid"

unshareNet :: BubbleM ()
unshareNet = unshareOpt "net"

unshareUts :: BubbleM ()
unshareUts = unshareOpt "uts"

unshareCgroup :: BubbleM ()
unshareCgroup = unshareOpt "cgroup"

unshareCgroupTry :: BubbleM ()
unshareCgroupTry = unshareOpt "cgroup-try"

unshareAll :: BubbleM ()
unshareAll = unshareOpt "all"

useUID :: UserID -> BubbleM ()
useUID uid = opt1 "uid" (show uid) 

useGID :: GroupID -> BubbleM ()
useGID gid = opt1 "uid" (show gid) 

useHostname :: String -> BubbleM ()
useHostname = opt1 "hostname"

-- Options about environment setup:

chdir :: FilePath -> BubbleM ()
chdir path = tell [ "--chdir", path ]

setEnv :: String -> String -> BubbleM ()
setEnv = opt2 "setenv"

unsetEnv :: String -> BubbleM ()
unsetEnv = opt1 "unsetenv"

-- Options for monitoring the sandbox from the outside:

lockFile :: FilePath -> BubbleM ()
lockFile = opt1 "lock-file"

syncFd :: Fd -> BubbleM ()
syncFd fd = opt1 "sync-fd" (show fd)

-- Filesystem related options:

bind :: FilePath -> FilePath -> BubbleM ()
bind = opt2 "bind"

devBind :: FilePath -> FilePath -> BubbleM ()
devBind = opt2 "dev-bind"

roBind :: FilePath -> FilePath -> BubbleM ()
roBind = opt2 "ro-bind"

remountRo :: FilePath -> BubbleM ()
remountRo = opt1 "remount-ro"

proc :: FilePath -> BubbleM ()
proc = opt1 "proc" 

dev :: FilePath -> BubbleM ()
dev = opt1 "dev"

tmpfs :: FilePath -> BubbleM ()
tmpfs = opt1 "tmpfs"

mqueue :: FilePath -> BubbleM ()
mqueue = opt1 "mqueue"

dir :: FilePath -> BubbleM ()
dir = opt1 "dir"

file :: Fd -> FilePath -> BubbleM ()
file fd = opt2 "file" (show fd)

bindData :: Fd -> FilePath -> BubbleM ()
bindData fd = opt2 "bind-data" (show fd)

roBindData :: Fd -> FilePath -> BubbleM ()
roBindData fd = opt2 "ro-bind-data" (show fd)

symlink :: FilePath -> FilePath -> BubbleM ()
symlink = opt2 "symlink"

-- Lockdown Options:

seccomp :: Fd -> BubbleM ()
seccomp fd = opt1 "seccomp" (show fd)

execLabel :: SeLabel -> BubbleM ()
execLabel = opt1 "exec-label"

fileLabel :: SeLabel -> BubbleM ()
fileLabel = opt1 "file-label"

blockFd :: Fd -> BubbleM ()
blockFd fd = opt1 "block-fd" (show fd)

infoFd :: Fd -> BubbleM ()
infoFd fd = opt1 "info-fd" (show fd)

newSession :: BubbleM ()
newSession = tell $ pure "--new-session"

dieWithParent :: BubbleM ()
dieWithParent = tell $ pure "--die-with-parent"


